import './App.css';

// components
import AppNavbar from "./components/AppNavbar";

// pages
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Dashboard from "./pages/Dashboard";
import CreateNewProduct from "./pages/CreateNewProduct";
import EditProduct from "./pages/EditProduct";
import Error from "./pages/Error";

// react bootstrap and react router dom
import { Route, Routes } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Container } from "react-bootstrap";

//
import { useState } from "react";
import { UserProvider } from "./UserContext";


function App() {

    // User state for global scope
    const [user, setUser] = useState({
        id : localStorage.getItem("id"),
        isAdmin : localStorage.getItem("isAdmin"),
        email : localStorage.getItem("email"),
        token : localStorage.getItem("token")
    })

    // For clearing the local storage on logout
    const unsetUser = () => {
        localStorage.clear();
    } 

    return (
        <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
                <AppNavbar/>
                <Routes>
                    <Route path="/" element={<Home/>} />
                    <Route path="/products" element={<Products/>} />
                    <Route path="/productView/:productId" element={<ProductView/>} />
                    <Route path="/dashboard" element={<Dashboard/>} />
                    <Route path="/createNewProduct" element={<CreateNewProduct/>} />
                    <Route path="/editProduct/:productId" element={<EditProduct/>} />
                    <Route path="/register" element={<Register/>} />
                    <Route path="/login" element={<Login/>} />
                    <Route path="/logout" element={<Logout/>} />
                    <Route path="*" element={<Error/>} />
                </Routes>
            </Router>
        </UserProvider>
    )
}

export default App;
