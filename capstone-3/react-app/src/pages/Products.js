import ProductCard from "../components/ProductCard";
import { useState, useEffect } from "react";
import { Row, Container, Col } from "react-bootstrap";



export default function Products(){

    const [ActiveProducts, setActiveProducts] = useState([]);

    // retrieve the products from db
    useEffect(() => {
        fetch(`https://capstone-2-w4lw.onrender.com/products`)
        .then(res => res.json())
        .then(data => {

            setActiveProducts(data.map(product => {
                return (

                    <ProductCard key={product._id} productProp={product} />

                )
            }))

        })
    }, [])

    return (

        <>
        {/*<Container fluid className="artwork-catalog p-5">
            <Row className="py-2">
                <Col className="px-5">
                    <p className="sale-text">Special Offer</p>
                    <h1 className="sale-title">S A L E</h1>
                    <h3 className="sale-subtitle"><strong>Shop Now</strong> and <strong>Save</strong> Up to <strong>50%</strong> for a <strong>Limited Time</strong>!</h3>
                </Col>
            </Row>
        </Container>*/}
        <Container>
            <h1 className="catalog-title pt-5 text-center">PRODUCTS</h1>
            <h3 className="catalog-intro pt-4 px-5 text-center">From luxurious designer scents to everyday favorites, we've got something for everyone. Don't miss out on this amazing opportunity to indulge your senses and save big!</h3>
            <Row>
                {ActiveProducts}
            </Row>
        </Container>
        </>

    )
}
