import {useState, useContext, useEffect} from "react";
import Swal from "sweetalert2";
import {Container, Form, Button} from "react-bootstrap";
import {useNavigate, Navigate} from "react-router-dom";
import UserContext from "../UserContext";

export default function CreateNewProduct(){

    const {user} = useContext(UserContext);
    
    const navigate = useNavigate();

    // useState
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);
    const [image, setImage] = useState("");
    const [isButtonActive, setIsButtonActive] = useState(false);

    const addProduct = (event) => {
        event.preventDefault();

        if(localStorage.getItem("isAdmin") === "true"){

        fetch(`https://capstone-2-w4lw.onrender.com/products/`, {
            method: "POST",
            headers: {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                stock: stock,
                image: image
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "Product Added",
                    icon: "success",
                    text: "You have successfully added a new product!"
                })

                navigate("/dashboard");

            }else{
                Swal.fire({
                    title: "Add Product Failed",
                   icon: "error",
                    text: "The system is experiencing trouble at the moment. Please try again later."
                })
            }
        })

        setName("");
        setDescription("");
        setPrice(0);
        setStock(0);
        setImage("");

        }else{
            navigate("/");
        }

    }

    useEffect(() => {
        if(name !== "" && description !== "" && price !== "" && image !== ""){
            setIsButtonActive(true);
        }else {
            setIsButtonActive(false);
        }
    }, [name, description, price, image]);

    return (
        localStorage.getItem("isAdmin") === "true" ?

        <Container className="p-5">
            <h1 className="text-center pb-4 create-product-heading">ADD NEW PRODUCT</h1>
            <Form className="add-product-form" onSubmit={event => addProduct(event)}>

              <Form.Group className="mb-3" controlId="name">
                <Form.Label className="add-product-label">Product Name</Form.Label>
                <Form.Control className="add-product-input" type="text" placeholder="Product name" value={name} onChange={event => setName(event.target.value)} />
              </Form.Group>

              <Form.Group className="mb-3" controlId="description">
                <Form.Label className="add-product-label">Product Description</Form.Label>
                <Form.Control className="add-product-input" type="text" placeholder="Product description" value={description} onChange={event => setDescription(event.target.value)} />
              </Form.Group>

              <Form.Group className="mb-3" controlId="price">
                <Form.Label className="add-product-label">Price</Form.Label>
                <Form.Control className="add-product-input" type="number" placeholder="Product price" value={price} onChange={event => setPrice(event.target.value)} />
              </Form.Group>

              <Form.Group className="mb-3" controlId="stock">
                <Form.Label className="add-product-label">Stock</Form.Label>
                <Form.Control className="add-product-input" type="number" placeholder="Product stock" value={stock} onChange={event => setStock(event.target.value)} />
              </Form.Group>

              <Form.Group className="mb-3" controlId="image">
                <Form.Label className="add-product-label">Product Image</Form.Label>
                <Form.Control className="add-product-input" type="text" placeholder="Product image URL" value={image} onChange={event => setImage(event.target.value)} />
              </Form.Group>

              {
                isButtonActive ?
                <Button className="newproduct-btn" variant="primary" type="submit">
                    Add Product
                </Button>
                :
                 <Button className="newproduct-btn" variant="primary" type="submit" disabled>
                    Add Product
                </Button>
              }
             

            </Form>
        </Container>

        :
        <Navigate to="/"/>
    )
}
