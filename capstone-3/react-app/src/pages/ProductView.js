import UserContext from "../UserContext";
import {useContext, useState, useEffect} from "react";
import {Button, Row, Col, Container, Form} from "react-bootstrap";
import {useParams, useNavigate, Navigate, Link, useHistory} from "react-router-dom";
import Swal from "sweetalert2";

export default function ProductView(){

    // contains the user details from the local storage.
    const {user} = useContext(UserContext);

    // allows to retrieve the productId passed via URL
    const {productId} = useParams();

    // where to navigate after successfully buying product/artwork
    const navigate = useNavigate();

    // useState
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");
    const [quantity, setQuantity] = useState(1);
    const [image, setImage] = useState("");

    // fetch the data of certain product
    useEffect(() => {

        console.log(productId)

         fetch(`https://capstone-2-w4lw.onrender.com/products/${productId}`)
        .then(res => res.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setImage(data.image);

            console.log(data);
        })
    }, [productId])


    // function to create an order
    const buy = (productId) => {
        if(localStorage.getItem("isAdmin") === "true"){
            Swal.fire({
                title: "OOPS!",
                icon: 'error',
                text: "Admin cannot perform this action."
            });

            navigate("/");

            console.log(localStorage.getItem("isAdmin"));
        }else{
           fetch(`https://capstone-2-w4lw.onrender.com/users/checkout`, {
                method: "POST",
                headers: {
                    "Content-type" : "application/json",
                    Authorization : `Bearer ${localStorage.getItem("token")}`
                },
                body: JSON.stringify({
                    productId: productId,
                    quantity: quantity
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);


                if(data === true){
                    Swal.fire({
                        title: "Order Created",
                        icon: 'success',
                        text: "Thank you for your order!"
                    });

                navigate("/products");

                }else{
                    Swal.fire({
                        title: "Create Order Failed",
                        icon: 'error',
                        text: "Something went wrong. Please try again!"
                    });
                }
            })
        }
    }


    return (

        <Container className="my-5 product-view">
            <Row>
                <Col sm={12} md={6} className="p-5">
                    <img className="img-fluid product-view-img my-4" src={image} />
                </Col>

                <Col sm={12} md={6} className="p-5 product-view-details">
                    <h2 className="mb-5 prodName">{name}</h2>
                    <h5 className="prodDesc">Description:</h5>
                    <p className="prodDesc-text">{description}</p>
                    <h5 className="prodPrice">Price:</h5>
                    <p className="prodPrice-text">Php {price}</p>
                    {
                
                        (user.id !== null)
                        ?
                        <>
                            <h5 className="prodQuant">Quantity:</h5>
                            <Form.Control className="prodQuant-text" type="number" placeholder="Quantity" value={quantity} onChange={event => setQuantity(event.target.value)} />
                            <Button id="checkOutBtn" className='mt-4 shadow product-view-button' onClick={() => buy(productId)}>CHECK OUT</Button>
                        </>
                        :
                            
                            <Button className='mt-4 shadow product-view-button' as={Link} to={`/login`}>LOGIN TO BUY</Button>
                    }
                </Col>
            </Row>
        </Container>
    )
}
