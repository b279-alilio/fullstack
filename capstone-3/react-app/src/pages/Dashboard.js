import UserContext from "../UserContext";
import {useContext, useState, useEffect} from "react";
import {Button, Container, Table, Row, Col} from "react-bootstrap";
import Swal from "sweetalert2";
import Banner from "../components/Banner";
import {Link, Navigate, useNavigate} from "react-router-dom";


export default function Dashboard(){

    const {user} = useContext(UserContext);

    const navigate = useNavigate();

    // useState
    const [allProducts, setAllProducts] = useState([]);
    const [productId, setProductId] = useState("");
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);
    const [isActive, setIsActive] = useState(false);

    // Get all products
    const getAllProducts = () => {
         fetch(`https://capstone-2-w4lw.onrender.com/products/all`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(localStorage.getItem("isAdmin") === "true"){

            setAllProducts(data.map(product => {
                return (
                    <>
                        <tr key={product._id}>
                            <td  className="table-body">{product._id}</td>
                            <td  className="table-body">{product.name}</td>
                            <td>{product.description}</td>
                            <td  className="table-body">{product.price}</td>
                            <td  className="table-body">{product.stock}</td>
                            <td  className="table-body">{product.isActive ? "Active" : "Inactive"}</td>
                            <td>
                                {
                                    (product.isActive) ?
                                        <Button className="archive-button" onClick={() => archive(product._id)}>
                                            Archive
                                        </Button>
                                    :
                                        <>
                                        <Button className="unarchive-button" variant="success" onClick={()=>unarchive(product._id)}>
                                            Unarchive
                                        </Button>

                                        <Button className="edit-button mt-2" as={Link} to={`/editProduct/${product._id}`}>
                                            Edit Product
                                        </Button>
                                        </>
                                }
                            </td>
                        </tr>
                    </>
                )
            }))
            }else{
                navigate("/");
            }
        })
    }

    useEffect(() => {
        getAllProducts()
    }, [])

    // Archive product
    const archive = (id) => {
        // console.log(id)
        fetch(`https://capstone-2-w4lw.onrender.com/products/${id}/archive`, {
            method: "PUT",
            headers: {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                // console.log(data)
                Swal.fire({
                    title: "Product Archived",
                    icon: "success",
                    text: "You have successfully deactivated this product!"
                })

                getAllProducts();

            }else{
                Swal.fire({
                    title: "Product Archiving Failed",
                    icon: "error",
                    text: "Something went wrong. Please try again."
                })
            }
        })
    }

    // Unarchive product
    const unarchive = (id) => {
        fetch(`https://capstone-2-w4lw.onrender.com/products/${id}/unArchive`, {
            method: "PUT",
            headers: {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: true
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "Product Unarchived",
                    icon: "success",
                    text: "You have successfully reactivated this product!"
                })

                getAllProducts();

            }else{
                Swal.fire({
                    title: "Product Unarchiving Failed",
                    icon: "error",
                    text: "Something went wrong. Please try again."
                })
            }
        })
    }

    // Dashboard banner
    const dashboardBanner = {
        title: "Hi there, Admin!",
        content: "How can I help you today?",
        buttonDestination: "/createNewProduct",
        buttonLabel: "Add New Product"
    }

    return(
        localStorage.getItem("isAdmin") === "true" ?
        <>
        <Container fluid className="p-2 p-md-4 p-lg-5 dashboard-page">
            <Row className="p-2 p-md-4 p-lg-5 min-vh-100 dashboardBanner-container">
                <Col className="pt-5 p-2 p-md-4 p-lg-5 text-center">
                    <Banner className="dashboard-banner-text" bannerProps={dashboardBanner}/>
                    <Button href="#dashboard-table" variant="secondary" type="submit" className="ms-5 view-products-btn">
                                                View/Manage Products
                </Button>
                </Col>
            </Row>
        </Container>
        <Container id="dashboard-table" sm={12}>
        <Row className="table-container">
            <h1 className="text-center all-products">ALL PRODUCTS</h1>
            <Col className="dashboard-table my-5">
                <Table className="table">
                <thead>
                    <tr>
                        <th className="table-header">ID</th>
                        <th className="table-header">Name</th>
                        <th className="table-header">Description</th>
                        <th className="table-header">Price</th>
                        <th className="table-header">Stocks</th>
                        <th className="table-header">Status</th>
                        <th className="table-header">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {allProducts}
                </tbody>
            </Table>
            </Col>
        </Row>  
        </Container>
        </>
        :
        <Navigate to="/" />
    )
}
