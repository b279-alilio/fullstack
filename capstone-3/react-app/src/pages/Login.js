import { Form, Button, Container, Row, Col } from "react-bootstrap";
import UserContext from "../UserContext";
import { useContext, useState, useEffect } from "react";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";


export default function Login(){

    const {user, setUser} = useContext(UserContext);

    // useState
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isButtonActive, setIsButtonActive] = useState(false);

    // Validation to enable submit button when all fields are populated with both passwords match
    useEffect(() => {
        if(email !== "" && password !== ""){
            setIsButtonActive(true);
        }else {
            setIsButtonActive(false);
        }
    }, [email, password])

    function loginUser(event) {

        // prevent page redirection upon form submission
        event.preventDefault();

        // login user
        fetch(`https://capstone-2-w4lw.onrender.com/users/login`, {
            method: "POST",
            headers: {
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data.access !== undefined){
                localStorage.setItem("token", data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful!",
                    icon: "success",
                    text: "Welcome to Scentsation!!"
                })
            }else {
                Swal.fire({
                    title: "Authentication Failed!",
                    icon: "error",
                    text: "Please try again!"
                })
            }
        })

        // Clear input fields after logging in
        setEmail("");
        setPassword("")
    }


    // Retrieve user details using user token
    const retrieveUserDetails = (token) => {
        fetch(`https://capstone-2-w4lw.onrender.com/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {

            /*id: localStorage.setItem("id", data._id),
            firstName: localStorage.setItem("firstName", data.firstName),
            lastName: localStorage.setItem("lastName", data.lastName),
            email: localStorage.setItem("email", data.email),
            isAdmin: localStorage.setItem("isAdmin", data.isAdmin)*/

            setUser({
                id: localStorage.setItem("id", data._id),
                firstName: localStorage.setItem("firstName", data.firstName),
                lastName: localStorage.setItem("lastName", data.lastName),
                email: localStorage.setItem("email", data.email),
                isAdmin: localStorage.setItem("isAdmin", data.isAdmin)
            })
        })
    }

    return(
        (user.token !== null) ?
            <Navigate to="/dashboard" />
        :
        <>
        <Container fluid>
            <Row className="login-bg min-vh-100">
            <Col className="login-img d-sm-0 px-5 py-4 order-md-2" md={12} lg={6}>
            </Col>
            <Col className="d-sm-0 px-5 py-4 order-md-2" md={12} lg={6}>
                <h1 className='display-1 text-center'>LOGIN NOW</h1>
                <h1 className='display-5 text-center'>Grab 'em by the scent!</h1>
                <Form className="login-form" onSubmit={event => loginUser(event)}>

                  <Form.Group controlId="userEmail">
                    <Form.Label className="pb-0 mb-0 loginform-label">Email address</Form.Label>
                    <Form.Control className="loginform-input" type="email" placeholder="Your email address" value={email} onChange={event => setEmail(event.target.value)} required />
                  </Form.Group>

                  <Form.Group controlId="password" className="mt-2">
                    <Form.Label className="pb-0 mb-0 loginform-label">Password</Form.Label>
                    <Form.Control className="loginform-input"  type="password" placeholder="Your password" value={password} onChange={event => setPassword(event.target.value)} required />
                  </Form.Group>

                  {
                    isButtonActive ?
                        <Button variant="primary" type="submit" className="mt-4 login-btn">
                            Login
                        </Button>
                    :
                        <Button variant="secondary" type="submit" disabled className="mt-4 login-btn">
                            Login
                        </Button>
                  }
                     
                </Form>
            </Col>
            </Row>
        </Container>
        </>
    )
}
