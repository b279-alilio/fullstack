// import/s from react-bootstrap
import { Form, Button, Container, Row, Col } from "react-bootstrap";

// import/s from react-router-dom
import { Navigate, useNavigate } from "react-router-dom";

// import/s from react
import { useState, useEffect, useContext } from "react";

// import from UserContext
import UserContext from "../UserContext";

// import from sweetalert2
import Swal from "sweetalert2";


export default function Register(){

    const {user} = useContext(UserContext);

    const navigate = useNavigate();

    // useState
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isButtonActive, setIsButtonActive] = useState("");

    // Simulate user registration
    function registerUser(event){

        // prevent page redirection upon form submission
        event.preventDefault();

        // checks if user email already exist
        fetch(`https://capstone-2-w4lw.onrender.com/users/checkEmail`, {
            method : "POST",
            headers : {
                "Content-Type" : "application/json"
            },
            body : JSON.stringify({
                email : email
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title : "Duplicate email found",
                    icon : "error",
                    text : "Please provide a different email."
                })
            }else {
                // registers user and add user details to db.
                fetch(`https://capstone-2-w4lw.onrender.com/users/register`, {
                    method : "POST",
                    headers : {
                        "Content-Type" : "application/json"
                    },
                    body : JSON.stringify({
                        firstName : firstName,
                        lastName : lastName,
                        email : email,
                        mobileNumber : mobileNumber,
                        password : password
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if(data === true){

                        Swal.fire({
                            title : "Thank you for registering!",
                            icon : "success",
                            text : "Welcome to Scentsation!"
                        })

                        // Clear the input fields.
                        setFirstName("");
                        setLastName("");
                        setEmail("");
                        setMobileNumber("");
                        setPassword("");
                        setConfirmPassword("");

                        navigate("/login");

                    }else {
                        Swal.fire({
                            title : "Oops! Something went wrong.",
                            icon : "error",
                            text : "Please try again."
                        })
                    }
                })
            }
        })
    }

    // Validation to enable submit button when all fields are populated with both passwords match
    useEffect(() => {
        if((firstName !== "" && lastName !== "" && email !== "" && mobileNumber !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword)){
            setIsButtonActive(true);
        }else {
            setIsButtonActive(false);
        }
    }, [firstName, lastName, email, mobileNumber, password, confirmPassword]);

    return(
        (user.token !== null) ?
            <Navigate to="/" />
        :
        <>
        <Container fluid>
            <Row className="register-bg min-vh-100">
                {/*register form*/}
                <Col className="d-sm-0 px-5 py-4 order-md-2" md={12} lg={4}>
                    <h3 className="register-here text-center form-title">REGISTER HERE</h3>

                    <Form className="reg-form" onSubmit={event => registerUser(event)}>
                      <Form.Group controlId="firstName">
                        <Form.Label className="pb-0 mb-0 regform-label">First Name</Form.Label>
                        <Form.Control className="regform-input" type="text" placeholder="Your first name" value={firstName} onChange={event => setFirstName(event.target.value)} required />
                      </Form.Group>

                      <Form.Group controlId="lastName" className="mt-2">
                        <Form.Label className="pb-0 mb-0 regform-label">Last Name</Form.Label>
                        <Form.Control className="regform-input" type="text" placeholder="Your last name" value={lastName} onChange={event => setLastName(event.target.value)} required />
                      </Form.Group>

                      <Form.Group controlId="userEmail" className="mt-2">
                        <Form.Label className="pb-0 mb-0 regform-label">Email</Form.Label>
                        <Form.Control className="regform-input" type="email" placeholder="Your email address" value={email} onChange={event => setEmail(event.target.value)} required />
                      </Form.Group>

                      <Form.Group controlId="mobileNumber" className="mt-2">
                        <Form.Label className="pb-0 mb-0 regform-label">Mobile Number</Form.Label>
                        <Form.Control className="regform-input" type="text" placeholder="Your mobile number" value={mobileNumber} onChange={event => setMobileNumber(event.target.value)} required />
                      </Form.Group>

                      <Form.Group controlId="password" className="mt-2">
                        <Form.Label className="pb-0 mb-0 regform-label">Password</Form.Label>
                        <Form.Control className="regform-input" type="password" placeholder="Your password" value={password} onChange={event => setPassword(event.target.value)} required />
                      </Form.Group>

                      <Form.Group controlId="confirmPassword" className="mt-2">
                        <Form.Label className="pb-0 mb-0 regform-label">Confirm Password</Form.Label>
                        <Form.Control className="regform-input" type="password" placeholder="Confirm password" value={confirmPassword} onChange={event => setConfirmPassword(event.target.value)} required />
                      </Form.Group>

                      {
                        isButtonActive ?
                            <Button type="submit" className="mt-4 reg-btn">
                                Register
                            </Button>
                        :
                            <Button type="submit" disabled className="mt-4 reg-btn">
                                Register
                            </Button>
                      }
                     
                    </Form>
                    
                </Col>
                <Col className="register-img d-sm-0 px-5 py-4 order-md-2" md={12} lg={8}>
                </Col>
            </Row>
        </Container>
        
            
        </>
    )
}
