import Banner from "../components/Banner"
import {Container, Row, Col, Card, Button} from "react-bootstrap";
import {Link} from "react-router-dom";


export default function Home(){

    const bannerContent = {
        title: "Scentsation Perfumes",
        content: "Be More Scent-sational",
        buttonDestination: "/products",
        buttonLabel: "Find Your Scent"
    }

    return (

        <>
        {/*landing Page*/}
        <Container fluid className="p-2 p-md-4 p-lg-5 landing-page">
            <Row className="p-2 p-md-4 p-lg-5 landing-text-container">
                <Col className="pt-5 p-2 p-md-4 p-lg-5 text-center">
                    <Banner bannerProps={bannerContent}/>
                </Col>
            </Row>
        </Container>

        {/*Highlights*/}
        <Container fluid className="highlight">
        <Row className="cardHighlight mt-3 mb-3">
            {/*First Card*/}
            <Col xs={12} md={3}>
                <Card className="cardHighlight1 p-3">
                    <Card.Body className="cardBody1">
                        <Card.Title>Same Day Free Shipping</Card.Title>
                        <Card.Text>Orders ship on the day that you place them and arrive within days.</Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            {/*Second Card*/}
            <Col xs={12} md={3}>
                <Card className="cardHighlight2 p-3">
                    <Card.Body className="cardBody2">
                        <Card.Title>Trusted Since 2010</Card.Title>
                        <Card.Text>100% authentic fragrances. You won't find knockoffs or imitations here.</Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            {/*Third Card*/}
            <Col xs={12} md={3}>
                <Card className="cardHighlight3 p-3">
                    <Card.Body className="cardBody3">
                        <Card.Title>Extensive Product Range</Card.Title>
                        <Card.Text>Discover a wide selection of designer fragrances for men and women on our shopping site.</Card.Text>
                    </Card.Body>
                </Card>
            </Col>

             {/*Fourth Card*/}
            <Col xs={12} md={3}>
                <Card className="cardHighlight4 p-3">
                    <Card.Body className="cardBody4">
                        <Card.Title>Safe & Secure Checkout</Card.Title>
                        <Card.Text>100% safe and secure checkout. Your information is protected.</Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
        </Container>
                    
        </>

    )
}
