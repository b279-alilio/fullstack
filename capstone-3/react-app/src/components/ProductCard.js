import { Row, Col, Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function ProductCard({productProp}){
    
    const { _id, name, description, price, stock, image } = productProp;

    return(

        
        <>
        <Col className="my-5" xs={12} md={6} lg={3}>
            <Card className="productCard">
              <Card.Img variant="top" src="img-fluid product-img-fit w-100 py-3" src={image} />
              <Card.Body>
                <Card.Title className="card-title text-center">{name}</Card.Title>
                <Card.Text className="card-text text-center">Php {price}</Card.Text>
                
              </Card.Body>
              <Card.Footer>
                <Button  className="catalog-btn" variant="primary" as={Link} to={`/productView/${_id}`}>Details</Button>
              </Card.Footer>
            </Card>
        </Col>
        </>
        

    )
}
