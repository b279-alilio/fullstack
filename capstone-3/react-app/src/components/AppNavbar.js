import { Nav, Navbar, NavDropdown} from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import { useContext } from "react";
import UserContext from "../UserContext";

export default function AppNavbar(){

  const { user } = useContext(UserContext);
  const userFullName = `${localStorage.getItem("firstName")} ${localStorage.getItem("lastName")}`;
  console.log(user);

    return (

    <Navbar variant="dark" expand="lg" className="px-5 py-1 navbar-base" fixed="top">
        <Navbar.Brand className="navbar-brandname" as={Link} to={"/"}>Scentsation</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ms-auto nav-link">
                {
                    localStorage.getItem("isAdmin") === "true"
                    ?
                    <>
                    <Nav.Link className="px-3" as={NavLink} to="/dashboard" end>Dashboard</Nav.Link>
                    </>
                    :
                    <>
                    <Nav.Link className="px-3" as={NavLink} to={"/"}>Home</Nav.Link>
                    <Nav.Link className="px-3" as={NavLink} to={"/products"}>Products</Nav.Link>
                    </>
                }

                {
                  (user.token !== null) 
                  ?
                    <>
                    <NavDropdown title={userFullName} align="end" id="collasible-nav-dropdown" end>
                    <NavDropdown.Item as={ NavLink } to="/logout" end>Logout</NavDropdown.Item>
                    </NavDropdown>
                    </>
                  :
                    <>
                      <Nav.Link className="px-3" as={NavLink} to={"/register"}>Register</Nav.Link>
                      <Nav.Link className="px-3" as={NavLink} to={"/login"}>Login</Nav.Link>
                    </>
                }
              </Nav>
          </Navbar.Collapse>
      </Navbar>

    )
}
