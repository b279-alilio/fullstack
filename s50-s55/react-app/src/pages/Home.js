import Banner from "../components/Banner"
import Highlights from "../components/Highlights"


export default function Home(){

	const data = {
	title: "Scentsation Perfumes",
	content: "Let your scent be a little bit more... Scent-sational.",
	destination: "/products",
	label: "Discover your new signature scent!"
	}

	return(
			<>
				<Banner bannerProps={data} />
				<Highlights/>
			</>
		)
}