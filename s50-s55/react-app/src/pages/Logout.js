import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import { useState, useContext, useEffect} from 'react';

export default function Logout(){

	// localStorage.clear();

	const {unsetUser, setUser} = useContext(UserContext);

	unsetUser();

	useEffect(() => {
		setUser({
			id: null,
      		isAdmin: null,
      		email: null,
      		token: null
		})
	})

	return(
		<Navigate to="/login" />
		);
}