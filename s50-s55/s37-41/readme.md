# S32 - Booking System API - Business Use Case Translation to Model Design
## Topics
* Booking System MVP requirements
* Model Definition for Users and Courses
* Booking System API Dependencies
## Presentation
[S32 Presentation](https://docs.google.com/presentation/d/1FgvQ-Och2oAlLNy-n_kwuVL4Dz7K__qUOV_cI2CcKHM/edit?usp=sharing)
## Codealong Guide:
1. Create project directory named bxx_booking. Navigate to this directory and initialize npm via the terminal command **npm init**.
* you can hit enter on all prompts that will show up to set them to their defaults

2. Install the npm packages that we will need for this project:
* [bcrypt](https://www.npmjs.com/package/bcrypt) - for hashing of user-submitted passwords
* [cors](https://www.npmjs.com/package/cors) - for allowing cross-origin resource sharing
* [express](https://www.npmjs.com/package/express) - our framework of choice for Node.js
* [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken) - provides helper methods in working with JWT's
* [mongoose](https://www.npmjs.com/package/mongoose) - for mapping our JS objects to MongoDB documents

3. Create the following subdirectories and files in our project directory:
* controllers (directory)
    * courseController.js (file)
    * userController.js (file)
* models (directory)
    * course.js (file)
    * user.js (file)
* routes (directory)
    * courseRoutes.js (file)
    * userRoutes.js (file)
* .gitignore (file) 
* app.js (file)
* auth.js (file)

4. Put **node_modules** in our .gitignore file to exclude the node modules of the packages we install in our git commits.

5. Wire up our app's components in its entry point - app.js, as follows:
```javascript
//Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors"); //This allows your sites to connect to this server
const courseRoutes = require("./routes/courseRoutes");
// ./ mean start looking from the current folder
const userRoutes = require("./routes/userRoutes"); //allow us to use the content of userRoutes.js

//Add the database connection
mongoose.connect("mongodb+srv://database-admin:admin_1234@sampledb.zxc7v.mongodb.net/b84_booking?retryWrites=true&w=majority", {useNewUrlParser: true});
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))

//Server setup
const app = express();
// const port = 3000;
app.use(cors()); //Tells the server that cors is being used by your server
app.use(express.json());
app.use(express.urlencoded({extended:true}))

//Add the routes
//course
app.use("/courses", courseRoutes);
//with this, all the routes in courses would start with /courses
//user
app.use("/users", userRoutes);
//with this line, all the routes related to our users start with /users
//ex. localhost:3000/users/register, localhost:3000/users/login, localhost:3000/users/details, etc.

//Listen to the server
// app.listen(port, () => console.log(`Listening to port ${port}`));
app.listen(process.env.PORT || 3000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 3000 }`)
});
//process.env.PORT is a port that can be assigned by your hosting service
//When we host, we move the server from the local computer to the web
//If the server is not hosted, it is going to connect to port 3000 instead.
```

6. Define the User model in models/user.js as follows:
```javascript
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	//Each user has the following fields: 
	//firstName, lastName, email, password, isAdmin (Boolean), mobileNo (String),
	//enrollments which is an array of courseId, enrolledOn, status
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	//Mini exercise: Continue putting the other fields except the enrollments.
	//Note: isAdmin is a Boolean that has a default value of false
	//Note 2: all fields are required.
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : { //Whenever a new user is created, it is not an admin
		type : Boolean,
		default : false //Why did I not put required here? It always has a value (which defaults to false)
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	enrollments : [ //this means that a user can have multiple enrollments, hence the need for an array, ex. a user can enroll in intro to js, and intro to mongo, and intro to express
		{
			courseId : {
				type : String,
				required : [true, "Course ID is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date()
			},
			status : {
				type : String,
				default : "Enrolled" //it can have other values like "Completed" or "Dropped"
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);
//module.exports allows us to use the file as a module, similar to packages, and can be used by other files
```

7. Define the Course model in models/course.js as follows:
```javascript
const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "Course is required"] //this line makes the field required and adds a message if the field is not present
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date() //new Date() is the current date when the course is created
	},
	enrollees : [ //this says that enrollees IS an array containing objects for userID and enrolledOn, it means that you can have multiple enrollees
		{
			userId : {
				type : String,
				required: [true, "UserId is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date() 
			}
		}
	]
})

module.exports = mongoose.model("Course", courseSchema);
```

# S33 - Booking System API - User Registration, Authentication, and JWT's
## Topics
* User Registration
* User Authentication
* JWT
## Presentation
[S33 Presentation](https://docs.google.com/presentation/d/15fsby3B4BTi0RlreQwd2kD_zMCDDK7MOjGNiZzyNFcg/edit?usp=sharing)
## Codealong Guide:
1. Define the logic for creating an access token in **auth.js** as follows:
* an access token will be generated by our API upon successful user authentication and will be sent as a response to the authentication request
```javascript
const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI"; //secret can be any string

//JSON Web Token or JWT is a way of securely passing information from a part of the server to the front end or to other parts of server
//Information is kept secure through the use of the secret
//Only the system that knows the secret can open the information

//Imagine JWT as a gift wrapping service but with a secret
//Only the person who knows the secret code can open the gift
//And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
//This ensures that the data is secure from the sender to the receiver

//it has 3 main parts
//creation of the token -> analogy: pack the gift, and sign with the secret
module.exports.createAccessToken = (user) => {
	//the data comes from the user parameter from the login
	//when the user logs in, a token will be created with data containing the user's information
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
		//Note you can add more information if you need
	}

	//create the token and using the jwt's sign function
	return jwt.sign(data, secret, {});
	//create a token from the data with secret and no additional parameters 
}
```
2. Define the controller actions for checking of duplicate emails, user registration, and user authentication (login) in /controllers/userController.js as follows:
```javascript
//get the dependencies
const User = require("../models/user");
//Course model will be needed for enrollment action
const Course = require("../models/course");
const auth = require("../auth"); //from the auth.js file and we use to authorize the requests
const bcrypt = require("bcrypt"); //is used to encrypt our passwords
//encryption is a way of hiding your data
//ex in facebook, the login page has your email and password
//ex. email: brandon@gmail.com password: brandon12345
//the value in the database for the password is not stored as brandon12345 but instead a randomized set of letters and numbers ex. iowsadlf521lk
//the information is hidden in the string of letters and numbers

//for the user, we have 5 functionalities
//check if the email already exists
/*
Steps: 
1. use mongoose.find function using the email
2. use then to send the result if there is an existing email or not
*/
module.exports.checkEmailExists = (body) => {
	return User.find({email : body.email}).then(result => {
		if (result.length > 0){
			return true; //meaning that the email already exists in the database
		} else {
			return false; //meaning that the email is not yet in the database
		}
	})
} //Ask the database if there is an email equivalent to the content of your body


//the registration process
/*
Steps:
1. Create a new User with the information from the body
2. Make sure that the password is encrypted
3. Save the new User to the database
*/
module.exports.registerUser = (body) => {
	let newUser = new User({
		firstName : body.firstName,
		lastName : body.lastName,
		email : body.email,
		mobileNo : body.mobileNo,
		password : bcrypt.hashSync(body.password, 10)
		//hashSync is a function of bcrypt that encrypts the password, and the 10 is number of times it runs the encryption
	})

	return newUser.save().then((user, error) => {
		if (error){
			return false; //user was not saved
		} else {
			return true; //user was saved
		}
	})
}


//the login process
module.exports.loginUser = (body) => {
	//findOne is an alternative to find that ensures only the first result is obtained
	//you can still use find, but we used findOne here to show you that there are different find functions
	return User.findOne({email : body.email}).then(result => {
		if(result == null){
			return false; //User doesn't exist
		} else {
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password);
			//compareSync() is used to compare a non encrypted password to the encrypted password and returns true or false depending on the result
			//a good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
			//ex. isSingle, isDone, isAdmin, areDone etc..

			//After comparing the passwords, we then check the value of isPasswordCorrect,
			//if it is true, then we create a token meaning that the user is logged in,
			//if it is false, then the passwords don't match and no token will be created
			if(isPasswordCorrect){ //if we are using boolean variables inside the if statement, we don't need to compare it to true or false
				return {access : auth.createAccessToken(result.toObject())}
				//auth is auth.js
				//createAccessToken is the function to create the token
				//.toObject() is a function that transforms the User into an Object format that can be used by our createAccessToken
			} else {
				return false; //meaning passwords don't match
			}
		}
	})
}
}
```

3. Define the routes for checking duplicate emails, registering users, and logging in users in /routes/userRoutes.js as follows:
```javascript
//setup the dependencies
const express = require("express");
const router = express.Router();
//mini exercise
//require auth.js and the userController file
const auth = require("../auth");
const userController = require("../controllers/userController");

//Route for check if email exists
//Hint: you are submitting information to the server
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

//Route for registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

//Route for login
//Mini exercise
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})
```
4. Run the server with the terminal command **node app.js**.

5. Import the provided **bxx_booking.postman_collection.json** in Postman and test the endpoints defined in out routes using the corresponding tests there.

# S34 - Booking System API - Authorization via JWT, Retrieval of User Details
## Topics
* Authorization
* Retrieval of Authenticated User's Details
## Presentation
[S34 Presentation](https://docs.google.com/presentation/d/1IN24Fx5PnrlIq4Bd_UIYwQJg93xqisWRRnH0HuRTrm0/edit?usp=sharing)
## Codealong Guide:
1. For authorization, we would need to do two things:
* **verify** the authenticity of the JWT sent in the request header
* **decode** the payload of the JWT to get its stored information
Define the following functions in the **auth.js** file:
```javascript
//verification of the token -> analogy: receive the gift and verify if the sender is legitimate and the gift was not tampered with
module.exports.verify = (req, res, next) => { //the next keyword tells the server to proceed if the verification is okay.
	let token = req.headers.authorization; //we put the JWT in the headers of the request.
	//in Postman, this is found in Authorization -> select "Bearer Token" -> Token value

	if(typeof token !== "undefined"){ //if the token is present

		console.log(token); //simply a check to see what the token's value is, can be removed later
		token = token.slice(7, token.length);
		//Recall slice function for JS - cuts the string starting from the 1st value up to the specified value
		//in this case, it starts from the 7th character to the end of the string
		//This is because the first 7 characters is not relevant to the actual token
		//ex. Bearer 7sdfkjjjkioqiwoieioklsdfjhkad
		//we don't need the word "Bearer " so we remove it using slice

		//after the token has been sliced, we then use jwt's verify function
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.send({auth : "failed"});
			} else {
				next();
			}
		})
		//jwt.verify verifies the token using the secret and fails if the token's secret does not match the secret phrase (meaning the token has been tampered with)
		//next() tells the server to allow us to proceed with the next request


	} else { //if the token is not present
		return res.send({auth : "failed"})
	}


}

//decoding of the token -> analogy : open the gift and get the content
module.exports.decode = (token) => {
	//check if the token is present of not
	if(typeof token !== "undefined"){ //there is a token present

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err){
				return null
			} else {
				return jwt.decode(token, {complete:true}).payload
				//jwt.decode -> decodes the token and get the payload
				//payload is the data from the token we create from createAccessToken
				//(the one with _id, the email, and the isAdmin)
			}
		})

	} else { //there is no token
		return null
	}
}
```

2. Define the controller action for getting the profile of an authenticated user in /controllers/userController.js as follows:
```javascript
//get the user profile
//uses the token as a way to verify is the user is logged in or not, such that this works only if the user is logged in. We're going to do that check in the routes later.
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "****"; //Q: Why did I do this? Not to show the encrypted password to the user.
		return result;
	})
}
```

3. Invoke this controller action in its corresponding route in /routes/userRoutes.js:
```javascript
//Route for getting the user's details
//auth.verify is the verify function in the auth.js
//auth.verify ensures that a user is logged in before proceeding to the next part of the code.
//as you cannot get the details of a user if you are not logged in
//auth.verify is what is called a middleware, and a middleware's job is to act like a gate.
//wherein only those with permission can proceed
router.get("/details", auth.verify, (req, res) => {
	//decode the token and get the content which already contains the user's ID
	//the req.headers.authorization contains the token that was created
	const userData = auth.decode(req.headers.authorization);

	//use the userData's ID for the getProfile function of the controller
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
	//the data that send is the ID so in the controller we set the parameter to data instead of the body

})
```

4. Restart the server to reflect the changes and test the endpoint for getting the details of an authenticated user via the following steps:
* simulate user login
* take the JWT sent as a response from successful login and place it in the request's Authorization header as a Bearer token  
![01](./screenshots/01.png)  

# S35 - Booking System API - Courses CRUD
## Topics
* Courses CRUD
## Presentation
[S35 Presentation](https://docs.google.com/presentation/d/1_CspcyqQ0SLYMuRBUgFMVgPAjXDs_9NF9X4sOscRYug/edit?usp=sharing)
## Codealong Guide:
1. Define the controller actions in /controllers/courseController.js as follows:
```javascript
const Course = require("../models/course"); 

//get all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
	//find all course that are active THEN put the result in a variable called result, then return the result
}
//the search criteria looks for the field isActive and checks if it is true or not
//If it is true then it is part of the result

//create a new course
module.exports.addCourse = (body) => { //the body corresponds to the req.body later in the routes.
	let newCourse = new Course({
		name : body.name,
		description : body.description,
		price : body.price
	});

	return newCourse.save().then((course, error) => {
		if (error) {
			return false; //save unsuccessful
		} else {
			return true; //save successfuls
		}
	})
}

//get a single course using the ID
module.exports.getCourse = (params) => { //the params correspond to the req.params later
	return Course.findById(params.courseId).then(result => {
		return result;
	})
}

//update a course
//we need information from the params and body
module.exports.updateCourse = (params, body) => {
	let updatedCourse = {
		name : body.name,
		description	: body.description,
		price : body.price
	}

	return Course.findByIdAndUpdate(params.courseId, updatedCourse).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})

	/* to update an entry, what we did last time was the following:
		we first used findById then modify the fields and save()

		With findByIdAndUpdate, we just need to specify two things.
		1. id of the course
		2. fields we want to update inside an object
	*/
}

//archive a course (change the isActive to false)
module.exports.archiveCourse = (params) => {
	let updateActiveField = {
		isActive : false
	}

	return Course.findByIdAndUpdate(params.courseId, updateActiveField).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}

//Mini exercise
//get all courses (both active and inactive)
module.exports.getAllCourses = () => {
	//fill this out.
	//Hint: use find with an empty search criteria
	return Course.find({}).then(result => {
		return result;
	})
}
```
2. Define the routes in /routes/courseRoutes as follows:
```javascript
//setup the dependencies
const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
//need to add auth.js as a dependency
const auth = require("../auth");
//Mini exercise:
//1. Identify the routes that need the middleware "auth.verify"
//2. Add the middleware

//setup the routes for functions

//route to get all active courses
router.get("/", (req, res) => { //no need for middleware
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
	//The route calls the controller's getAllActive function and returns the result as the response.
})

//Mini exercise
//route to get all the courses
router.get("/all", (req, res) => { //need for middleware
	//what controller function should i call?
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

//route to create a new course
router.post("/", auth.verify, (req, res) => { //need for middleware
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})

//route to get a single course
router.get("/:courseId", (req, res) => { //no need for middleware
	//the route is :courseId because in the controller, we specified that the params has courseId field
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

//route to update the details of a course
router.put("/:courseId", auth.verify, (req, res) => { //need for middleware
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	//Params:
	//ex. my url is localhost:3000/courses/1
	//my params in this case is params.courseId and its value is 1
})

//route to archive a course
router.put("/:courseId/archive", auth.verify, (req, res) => { //need for middleware
	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController))
	//My url for archiving would be http://localhost:3000/courses/1/archive
	//The params is courseId and its value is 1
})

//export the router
module.exports = router;
```
3. Test the endpoints following the collection of requests filed under **courses** in the Postman collection.

# S36 - Booking System API - User Course Enrollment, API Deployment
## Topics
* User Enrollment in Active Course
* Heroku Deployment
## Presentation
[S36 Presentation](https://docs.google.com/presentation/d/1-THXupwvcVskCFkQtYUUPi9ImuvTSV9rc8wP0mnxZDI/edit?usp=sharing)
## Codealong Guide:
1. In /controllers/userController.js, define the controller action for enrolling an authenticated user in a given course:
```javascript
//enroll in a class
module.exports.enroll = async (data) => {

	//Get the courseId and we save it in the enrollments array of the user
	let userSaveStatus = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId : data.courseId});
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	})
	//the userSaveStatus tell us if the course was successfully added to the user

	//Add the user to the enrollees of the course
	let courseSaveStatus = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId : data.userId});
		return course.save().then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		})
	})
	//the courseSaveStatus tells us if the user was successfully saved to the course

	//We have to check if the courseSaveStatus and userSaveStatus are both successful
	if(userSaveStatus && courseSaveStatus){
		return true;
	} else {
		return false;
	}

	//Async and await - allow processes to wait for each other.
	//In JS whenever we use the word "then", it needs a promise to be fulfilled.
	//Unfortunately, if we combine the code with the promise as well as code that doesn't need a promise, the one without the promise runs first.
	//Use async in the parent function telling that inside the function we can wait for other parts of the code to finish
	//Use await in the parts of the code that contain promises, such that the function would know to wait for these parts before running other sections of the code.
	//Imagine this scenario:
	//2 people are preparing for a meal, a chicken meal
	//1st person prepares the chicken
	//2nd person prepares the plate for the chicken
	//Without the async await, it might happen that the chicken is cooked first before the plate is ready and the 1st person will give the chicken to customer without the plate.
	//But with async await, the person with the chicken will wait for the person to finish preparing the plate before putting it and giving it to the customer
	//The promise in this example is that the person who is preparing the plate (is the server). 
	//The one without the promise is the person who is preparing the chicken


	//If the enroll function doesn't have the async await, the if statement will not wait for the course save and user save to finish and tell the route that it was unsuccessful
}
```

2. Define a route for user enrollment in /routes/userRoutes.js:
```javascript
//Route to enroll to a course
//The middleware ensures that the user is logged in before they can enroll
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		//userId comes from the decoded JWT's id field
		courseId : req.body.courseId
		//courseId comes from the request's body
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})
```

3. Test the enrollment endpoint via the Postman request named enroll in the exported Postman collection.

## Heroku Deployment Instructions
* can be shared to class via BooDLE Notes
1. Check if you have heroku CLI installed in your computer via the terminal command:
> heroku --version
2. Register an account with Heroku via this URL:
> https://signup.heroku.com/login
3. Create a file in the root directory of your project called **Procfile** with the following content: 
> web: node app
* the file has to start with an uppercase P
* the file should NOT have any file type extension
* the spaces in the file content are  
4. Login to Heroku via its CLI. Input the following in your terminal:
> heroku login
* you'll be redirected to the browser for authentication
5. Once logged in, create a heroku app with the following terminal command:
> heroku create
* must be done at project's root directory
* an app name will be auto-generated for you
* this will also automatically add a remote repository named heroku to your local repo, you can verify this with git remote -v
6. Get the application name of the newly created project
* a free-tier Heroku account only allows for **5** projects at any given time
7. After committing changes, push your app to Heroku with the terminal command:
> git push heroku master
* heroku will build your application based on the contents of your package.json file
* heroku will run your application based on the contents of your Procfile
8. To test your deployment, replace **localhost:3000** with your Heroku app's URL in Postman